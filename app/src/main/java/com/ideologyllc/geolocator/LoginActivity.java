package com.ideologyllc.geolocator;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.inputmethod.EditorInfo;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.ideologyllc.geolocator.location.LocationTraker;
import com.thingworx.common.RESTAPIConstants;
import com.thingworx.communications.client.AndroidConnectedThingClient;
import com.thingworx.communications.client.ClientConfigurator;
import com.thingworx.relationships.RelationshipTypes;
import com.thingworx.types.InfoTable;
import com.thingworx.types.collections.ValueCollection;
import com.thingworx.types.primitives.LocationPrimitive;
import com.thingworx.types.primitives.StringPrimitive;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Timer;
import java.util.TimerTask;

/**
 * A login screen that offers login via email/password.
 */
public class LoginActivity extends AppCompatActivity {

    /**
     * Keep track of the login task to ensure we can cancel it if requested.
     */
    private UserLoginTask mAuthTask = null;
    private int FLAG;
    private Timer timer;
    private TimerTask timerTask;
    final Handler handlerTask = new Handler();
    // UI references.
    private AutoCompleteTextView mUserNameView, phoneNumberView;
    private EditText mPasswordView;
    private View mLoginFormView;

    private String uri = "ws://23.228.149.221:80/Thingworx/WS";
    private String appKey = "0001f50b-fc24-4649-b7df-8aba824bcad6";
    protected AndroidConnectedThingClient client;
    protected ClientConfigurator config;
    private ProgressDialog connectionProgress;
    Context context;
    private String geoLocatorThingName;

    private String oldLatitude;
    private String oldLongitude;
    private String newLatitude;
    private String newLongitude;
    private String oldDate;
    private String currentDateandTime;

    private StringPrimitive newDatePrimitive;
    private StringPrimitive newLatitudePrimitive;
    private StringPrimitive newLongitudePrimitive;

    private TextView txtCurrentDate;
    private TextView txtCurrentLatitude;
    private TextView txtCurrentLongitude;
    private TextView txtOldDate;
    private TextView txtOldtLatitude;
    private TextView txtOldLongitude;
    private TextView txtLat, txtLon, txtMessage;
    private LinearLayout contentLayout, loginMainLayout, loginFullLayout;
    private Double latitude;
    private Double longitude;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        FLAG = 1;
        connectingToCloud();
        initialization();
    }


    private void initialization() {
        // Set up the login form.
        mUserNameView = (AutoCompleteTextView) findViewById(R.id.username);
        phoneNumberView = (AutoCompleteTextView) findViewById(R.id.phoneNumber);
        mPasswordView = (EditText) findViewById(R.id.password);
        mPasswordView.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView textView, int id, KeyEvent keyEvent) {
                if (id == R.id.login || id == EditorInfo.IME_NULL) {
                    attemptLogin();
                    return true;
                }
                return false;
            }
        });

        Button mUserSignInButton = (Button) findViewById(R.id.user_sign_in_button);
        mUserSignInButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                attemptLogin();
            }
        });

        mLoginFormView = findViewById(R.id.login_form);
        txtCurrentDate = (TextView) findViewById(R.id.textViewCurrentDate);
        txtCurrentLatitude = (TextView) findViewById(R.id.textViewCurrentLatitute);
        txtCurrentLongitude = (TextView) findViewById(R.id.textViewCurrentLongitute);
        txtOldDate = (TextView) findViewById(R.id.textViewOldDate);
        txtOldtLatitude = (TextView) findViewById(R.id.textViewOldLatitute);
        txtOldLongitude = (TextView) findViewById(R.id.textViewOldLongitute);
        txtLat = (TextView) findViewById(R.id.textViewLat);
        txtLon = (TextView) findViewById(R.id.textViewLon);
        txtMessage = (TextView) findViewById(R.id.textViewMessage);
        loginFullLayout = (LinearLayout) findViewById(R.id.loginFullLayout);
        contentLayout = (LinearLayout) findViewById(R.id.layoutContent);
        loginMainLayout = (LinearLayout) findViewById(R.id.loginMainLayout);

        context = this;
        getMyLocation();
        getCurrentTime();

    }

    private void connectingToCloud() {
        config = new ClientConfigurator();
        config.setUri(uri);
        config.setReconnectInterval(15);
        config.getSecurityClaims().addClaim(RESTAPIConstants.PARAM_APPKEY, appKey);
        config.ignoreSSLErrors(true);
        config.setConnectTimeout(10000);

        try {
            client = new AndroidConnectedThingClient(config);
            client.start();

        } catch (Exception e) {
            e.printStackTrace();
//            Toast.makeText(getApplicationContext(), "" + e, Toast.LENGTH_LONG).show();
        }
    }


    /**
     * Attempts to sign in or register the account specified by the login form.
     * If there are form errors (invalid email, missing fields, etc.), the
     * errors are presented and no actual login attempt is made.
     */
    private void attemptLogin() {

        if (mAuthTask != null) {
            return;
        }

        // Reset errors.
        mUserNameView.setError(null);
        mPasswordView.setError(null);

        // Store values at the time of the login attempt.
        String userName = mUserNameView.getText().toString();
        String password = mPasswordView.getText().toString();
        String phoneNumber = phoneNumberView.getText().toString();

        boolean cancel = false;
        View focusView = null;

        // Check for a valid password, if the user entered one.
        if (!TextUtils.isEmpty(password) && !isPasswordValid(password)) {
            mPasswordView.setError(getString(R.string.error_invalid_password));
            focusView = mPasswordView;
            cancel = true;
        }

        // Check for a valid userName address.
        if (TextUtils.isEmpty(userName)) {
            mUserNameView.setError(getString(R.string.error_field_required));
            focusView = mUserNameView;
            cancel = true;
        }
        // Check for a phone number.
        if (TextUtils.isEmpty(phoneNumber)) {
            phoneNumberView.setError(getString(R.string.error_field_required));
            focusView = phoneNumberView;
            cancel = true;
        }

        if (cancel) {
            // There was an error; don't attempt login and focus the first
            // form field with an error.
            focusView.requestFocus();
        } else {
            // Show a progress spinner, and kick off a background task to
            // perform the user login attempt.
            //showProgress(true);
            mAuthTask = new UserLoginTask(userName, password, phoneNumber);
        }
    }


    private boolean isPasswordValid(String password) {
        //TODO: Replace this with your own logic
        return password.length() > 3;
    }

    //Restart Activity
    private void restartActivity() {
        Intent intent = getIntent();
        finish();
        startActivity(intent);
    }


    /**
     * Represents a login/registration task used to authenticate
     * the user.
     */
    public class UserLoginTask {
        private String userName;
        private String password;
        private String phoneNumber;
        private boolean isValid = false;
        private InfoTable result;


        public UserLoginTask(String userName, String password, String phoneNumber) {
            this.userName = userName;
            this.password = password;
            this.phoneNumber = phoneNumber;
            validation();
        }


        public void validation() {
            loginFullLayout.setBackgroundColor(Color.parseColor("#000000"));
            contentLayout.setVisibility(View.GONE);
            loginMainLayout.setVisibility(View.GONE);
            connectionProgress = ProgressDialog.show(LoginActivity.this, null, "Please wait...");
            new Thread(new Runnable() {
                @Override
                public void run() {
                    try {
                        Thread.sleep(7000);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }

                    ValueCollection params = new ValueCollection();
                    params.put("user", new StringPrimitive(userName));
                    params.put("pass", new StringPrimitive(password));

                    try {
                        result = client.invokeService(RelationshipTypes.ThingworxEntityTypes.Things, "GeolocatorUtility", "authentication", params, 5000);
                        if (!result.isEmpty()) {
                            String state = result.getFirstRow().getStringValue("result");
                            Message msg = Message.obtain();
                            Bundle b = new Bundle();
                            b.putString("state", state);
                            msg.setData(b);
                            handlerValidation.sendMessage(msg);
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
//                        Toast.makeText(context, "Error"+e, Toast.LENGTH_LONG).show();
                        Message msg = Message.obtain();
                        Bundle b = new Bundle();
                        b.putString("state", "error");
                        msg.setData(b);
                        handlerValidation.sendMessage(msg);
                    }
                }
            }).start();
        }

        Handler handlerValidation = new Handler() {
            @Override
            public void handleMessage(Message msg) {
                super.handleMessage(msg);
                String state;
                state = msg.getData().getString("state");

                switch (state) {

                    case "invalid":
                        connectionProgress.dismiss();
                        Toast.makeText(getApplicationContext(), "Login Failed", Toast.LENGTH_LONG).show();
                        restartActivity();
                        break;

                    case "error":
                        connectionProgress.dismiss();
                        Toast.makeText(getApplicationContext(), "Problem with Login.", Toast.LENGTH_LONG).show();
                        restartActivity();
                        break;

                    default:
//                        connectionProgress.dismiss();
                        geoLocatorThingName = state;
                        try {
                            client.writeProperty(RelationshipTypes.ThingworxEntityTypes.Things, geoLocatorThingName, "phone", new StringPrimitive(phoneNumber), 5000);
//                            client.disconnect();
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                        isValid = true;

                        if (isValidate()) {
//                            Intent i = new Intent(LoginActivity.this, ShowLocations.class);
//                            i.putExtra("thingname", state);
//                            startActivity(i);
//                            i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
//                            finish();
                            afterConnection();
                        }
                        break;

                }

            }
        };

        public boolean isValidate() {
            return isValid;
        }

    }

    private void afterConnection() {
        receivingDataFromCloud();
    }

    private void receivingDataFromCloud() {

        new Thread(new Runnable() {
            @Override
            public void run() {

                try {
                    Thread.sleep(5000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                ValueCollection paramsForGeoLocatorProperty = new ValueCollection();
                try {
                    InfoTable result = client.invokeService(RelationshipTypes.ThingworxEntityTypes.Things, geoLocatorThingName, "lastLocation", paramsForGeoLocatorProperty, 5000);
                    if (!result.isEmpty()) {
                        oldDate = result.getFirstRow().getStringValue("date");
                        oldLatitude = result.getFirstRow().getStringValue("latitude");
                        oldLongitude = result.getFirstRow().getStringValue("longitude");

                        Message msg = Message.obtain();
                        Bundle b = new Bundle();
                        b.putString("state", "true");
                        msg.setData(b);
                        handlerReceivingData.sendMessage(msg);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                    Message msg = Message.obtain();
                    Bundle b = new Bundle();
                    b.putString("state", "false");
                    msg.setData(b);
                    handlerReceivingData.sendMessage(msg);
                }
            }
        }).start();
    }

    Handler handlerReceivingData = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            String state;
            state = msg.getData().getString("state");

            switch (state) {
                case "true":
                    connectionProgress.dismiss();
                    Toast.makeText(getApplicationContext(), "Login Successful", Toast.LENGTH_LONG).show();
                    loginMainLayout.setVisibility(View.GONE);
                    loginFullLayout.setBackgroundColor(Color.parseColor("#FFFFFF"));
                    getSupportActionBar().setTitle("Locations");
                    showValuesToDevice();
                    break;
                case "false":
                    connectionProgress.dismiss();
                    loginMainLayout.setVisibility(View.GONE);
                    loginFullLayout.setBackgroundColor(Color.parseColor("#FFFFFF"));
                    getSupportActionBar().setTitle("Locations");
                    Toast.makeText(getApplicationContext(), "Error in connection", Toast.LENGTH_LONG).show();
                    break;
            }
        }
    };


    private void sendingDataToCloud() {
        contentLayout.setVisibility(View.GONE);
        loginFullLayout.setBackgroundColor(Color.parseColor("#000000"));
        connectionProgress = ProgressDialog.show(LoginActivity.this, "Please wait", "Sending data to server...");
        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    Thread.sleep(5000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }

                ValueCollection paramsForLocation = new ValueCollection();
                paramsForLocation.put("date", newDatePrimitive);
                paramsForLocation.put("latitude", newLatitudePrimitive);
                paramsForLocation.put("longitude", newLongitudePrimitive);
                paramsForLocation.put("location", new LocationPrimitive(latitude, longitude, 0.00));

                try {
                    client.invokeService(RelationshipTypes.ThingworxEntityTypes.Things, geoLocatorThingName, "setLocationProperty", paramsForLocation, 5000);

                    Message msg = Message.obtain();
                    Bundle b = new Bundle();
                    b.putString("state", "true");
                    msg.setData(b);
                    handlerSendingData.sendMessage(msg);
                } catch (Exception e) {
                    e.printStackTrace();
                    Message msg = Message.obtain();
                    Bundle b = new Bundle();
                    b.putString("state", "false");
                    msg.setData(b);
                    handlerSendingData.sendMessage(msg);
//                    Toast.makeText(context, "Something went wrong!! please restart Geolocator again", Toast.LENGTH_LONG).show();
                }

            }
        }).start();
    }

    Handler handlerSendingData = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            String state;
            state = msg.getData().getString("state");

            switch (state) {
                case "true":
                    connectionProgress.dismiss();
                    Toast.makeText(getApplicationContext(), "Location sending Successful", Toast.LENGTH_LONG).show();
                    break;
                case "false":
                    connectionProgress.dismiss();

                    AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(context);
                    alertDialogBuilder.setTitle("Something went wrong!!");
                    alertDialogBuilder.setMessage("Please restart " + getString(R.string.app_name) + " again or press cancel and try again");
                    alertDialogBuilder.setPositiveButton("Restart", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            restartActivity();
                        }
                    });
                    alertDialogBuilder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            connectingToCloud();
                        }
                    });
                    AlertDialog alertDialog = alertDialogBuilder.create();
                    alertDialog.show();
//                    Toast.makeText(getApplicationContext(), "Something went wrong!! please restart " + getString(R.string.app_name) + " again", Toast.LENGTH_LONG).show();
                    break;
            }
            contentLayout.setVisibility(View.VISIBLE);
            loginFullLayout.setBackgroundColor(Color.parseColor("#FFFFFF"));
            connectionProgress.dismiss();

        }
    };


    //Getting Current Location
    private void getMyLocation() {
        LocationTraker traker = new LocationTraker(context);

        if (traker.getState()) {
            latitude = traker.getlat();
            longitude = traker.getLon();

            newLatitude = Double.toString(latitude);
            newLongitude = Double.toString(longitude);
            newLatitudePrimitive = new StringPrimitive(newLatitude);
            newLongitudePrimitive = new StringPrimitive(newLongitude);

        }
    }

    public void getCurrentTime() {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        currentDateandTime = sdf.format(new Date());
        newDatePrimitive = new StringPrimitive(currentDateandTime);
    }

    private void showValuesToDevice() {
        contentLayout.setVisibility(View.VISIBLE);
        txtCurrentDate.setText(currentDateandTime);
        txtCurrentLatitude.setText(newLatitude);
        txtCurrentLongitude.setText(newLongitude);
        if (oldDate.equals("no-value")) {
            txtOldDate.setVisibility(View.GONE);
            txtLat.setVisibility(View.GONE);
            txtLon.setVisibility(View.GONE);
            txtOldtLatitude.setVisibility(View.GONE);
            txtOldLongitude.setVisibility(View.GONE);
            txtMessage.setVisibility(View.VISIBLE);
            txtMessage.setText("Previous location is empty.");
        } else {
            txtOldDate.setText(oldDate);
            txtOldtLatitude.setText(oldLatitude);
            txtOldLongitude.setText(oldLongitude);
        }
    }

    public void reportLocation(View v) {
        if (FLAG == 1) {
            sendingDataToCloud();
            FLAG = 2;
        } else {
            Toast.makeText(context, "Current Location Already Reported", Toast.LENGTH_SHORT).show();
        }
        startTimer();
    }

    private void scheduleWork() {
        receivingDataFromCloudSchedule();
//        sendingDataToCloudSchedule();
    }


    public void initializeTimerTask() {

        timerTask = new TimerTask() {
            public void run() {

                //use a handler to run a toast that shows the current timestamp
                handlerTask.post(new Runnable() {
                    public void run() {
                        scheduleWork();
                    }
                });
            }
        };
    }

    public void startTimer() {
        //set a new Timer
        timer = new Timer();

        //initialize the TimerTask's job
        initializeTimerTask();

        //schedule the timer, after the first 5000ms the TimerTask will run every 10000ms
        timer.schedule(timerTask, 5000, 20000); //
    }
    public void stoptimer(){
        //stop the timer, if it's not already null
        if (timer != null) {
            timer.cancel();
            timer = null;
        }
    }


    private void receivingDataFromCloudSchedule() {

        new Thread(new Runnable() {
            @Override
            public void run() {

                try {
                    Thread.sleep(5000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                ValueCollection paramsForGeoLocatorProperty = new ValueCollection();
                try {
                    InfoTable result = client.invokeService(RelationshipTypes.ThingworxEntityTypes.Things, geoLocatorThingName, "lastLocation", paramsForGeoLocatorProperty, 5000);
                    if (!result.isEmpty()) {
                        oldDate = result.getFirstRow().getStringValue("date");
                        oldLatitude = result.getFirstRow().getStringValue("latitude");
                        oldLongitude = result.getFirstRow().getStringValue("longitude");

                        Message msg = Message.obtain();
                        Bundle b = new Bundle();
                        b.putString("state", "true");
                        msg.setData(b);
                        handlerReceivingDataSchedule.sendMessage(msg);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                    Message msg = Message.obtain();
                    Bundle b = new Bundle();
                    b.putString("state", "false");
                    msg.setData(b);
                    handlerReceivingDataSchedule.sendMessage(msg);
                }
            }
        }).start();
    }

    Handler handlerReceivingDataSchedule = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            String state;
            state = msg.getData().getString("state");

            switch (state) {
                case "true":
                    getMyLocation();
                    getCurrentTime();
                    showValuesToDevice();
                    sendingDataToCloudSchedule();
                    break;
                case "false":
                    Toast.makeText(getApplicationContext(), "Error in connection !! Refresh not working", Toast.LENGTH_LONG).show();
                    break;
            }
        }
    };

    private void sendingDataToCloudSchedule() {
        ValueCollection paramsForLocation = new ValueCollection();
        paramsForLocation.put("date", newDatePrimitive);
        paramsForLocation.put("latitude", newLatitudePrimitive);
        paramsForLocation.put("longitude", newLongitudePrimitive);
        paramsForLocation.put("location", new LocationPrimitive(latitude, longitude, 0.00));

        try {
            client.invokeService(RelationshipTypes.ThingworxEntityTypes.Things, geoLocatorThingName, "setLocationProperty", paramsForLocation, 5000);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onBackPressed() {
        stoptimer();
        super.onBackPressed();
    }
}


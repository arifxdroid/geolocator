package com.ideologyllc.geolocator.virtualthings;

import android.content.Context;
import android.provider.Settings;

import com.ideologyllc.geolocator.location.LocationTraker;
import com.thingworx.communications.client.ConnectedThingClient;
import com.thingworx.communications.client.things.VirtualThing;
import com.thingworx.metadata.annotations.ThingworxPropertyDefinition;
import com.thingworx.metadata.annotations.ThingworxPropertyDefinitions;
import com.thingworx.types.primitives.LocationPrimitive;

//
//@SuppressWarnings("serial")
//@ThingworxPropertyDefinitions(properties = {
//        @ThingworxPropertyDefinition(name = "deviceid", description = "Unique ID of device", baseType = "STRING", aspects = {"isPersistent:true", "isReadOnly:false"}),
//        @ThingworxPropertyDefinition(name = "location", description = "Current Location", baseType = "LOCATION", aspects = {"isPersistent:true", "isReadOnly:false"}),
//})


public class GeoLocatorVirtualThing extends VirtualThing {
    private static double latitude;
    private static double longitude;
    private Context context;

    public GeoLocatorVirtualThing(String name, String description, ConnectedThingClient client, Context context) {

        super(name, description, client);
        this.context = context;
        getMyLocation();
        initializeFromAnnotations();
    }

    @Override
    public void processScanRequest() throws Exception {

//        setProperty("latitude", latitude);
//        setProperty("longitude", longitude);

        updateSubscribedProperties(15000);
        updateSubscribedEvents(60000);
    }

    //Getting Current Location
    private void getMyLocation() {
        LocationTraker traker = new LocationTraker(context);

        if (traker.getState()) {
            latitude = traker.getlat();
            longitude = traker.getLon();

        }
    }

    public String getLatLon() {
        return latitude + "...." + longitude;
    }


}
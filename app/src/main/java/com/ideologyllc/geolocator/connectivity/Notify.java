package com.ideologyllc.geolocator.connectivity;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.support.v4.app.NotificationCompat;

/**
 * Created by ${Arif} on 11/25/2015.
 */
public class Notify {

    Context context;
    NotificationManager mNotificationManager;

    public Notify(Context context) {
        this.context = context;
        mNotificationManager =
                (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
    }

    public void setNotification(String contextTitle, String contextText, int smallIcon, PendingIntent pendingIntent, int id){

        NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(context)
                .setContentTitle(contextTitle)
                .setContentText(contextText)
                .setSmallIcon(smallIcon)
                .setLights(16711680, 1000, 1000);
        mBuilder.setDefaults(Notification.DEFAULT_SOUND);
        mBuilder.setDefaults(Notification.DEFAULT_LIGHTS);
        mBuilder.setContentIntent(pendingIntent);
        mBuilder.setAutoCancel(true);
        mNotificationManager.notify(id, mBuilder.build());
    }
}

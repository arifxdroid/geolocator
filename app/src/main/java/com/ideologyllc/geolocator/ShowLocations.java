package com.ideologyllc.geolocator;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.ideologyllc.geolocator.location.LocationTraker;
import com.ideologyllc.geolocator.thingworxactivity.ThingworxActivity;
import com.ideologyllc.geolocator.virtualthings.GeoLocatorVirtualThing;
import com.thingworx.common.RESTAPIConstants;
import com.thingworx.communications.client.AndroidConnectedThingClient;
import com.thingworx.communications.client.ClientConfigurator;
import com.thingworx.communications.client.things.VirtualThing;
import com.thingworx.communications.client.things.VirtualThingPropertyChangeEvent;
import com.thingworx.communications.client.things.VirtualThingPropertyChangeListener;
import com.thingworx.relationships.RelationshipTypes;
import com.thingworx.types.InfoTable;
import com.thingworx.types.collections.ValueCollection;
import com.thingworx.types.primitives.StringPrimitive;

import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class ShowLocations extends ThingworxActivity {

    private String uri = "ws://23.228.149.221:80/Thingworx/WS";
    private String appKey = "0001f50b-fc24-4649-b7df-8aba824bcad6";
    protected AndroidConnectedThingClient client;
    private ProgressDialog connectionProgress;
    Context context;
    private SharedPreferences sharedPreferences;
    private String geoLocatorThingName;

    private String oldLatitude;
    private String oldLongitude;
    private String newLatitude;
    private String newLongitude;
    private String oldDate;
    private String currentDateandTime;

    private StringPrimitive newDatePrimitive;
    private StringPrimitive newLatitudePrimitive;
    private StringPrimitive newLongitudePrimitive;

    private TextView txtCurrentDate;
    private TextView txtCurrentLatitude;
    private TextView txtCurrentLongitude;
    private TextView txtOldDate;
    private TextView txtOldtLatitude;
    private TextView txtOldLongitude;
    private TextView txtLat, txtLon, txtMessage;
    private LinearLayout mainLayout, layoutContent;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_show_locations);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        initialization();
        connectingToCloud();
    }

    private void initialization() {
        txtCurrentDate = (TextView) findViewById(R.id.textViewCurrentDate);
        txtCurrentLatitude = (TextView) findViewById(R.id.textViewCurrentLatitute);
        txtCurrentLongitude = (TextView) findViewById(R.id.textViewCurrentLongitute);
        txtOldDate = (TextView) findViewById(R.id.textViewOldDate);
        txtOldtLatitude = (TextView) findViewById(R.id.textViewOldLatitute);
        txtOldLongitude = (TextView) findViewById(R.id.textViewOldLongitute);
        txtLat = (TextView) findViewById(R.id.textViewLat);
        txtLon = (TextView) findViewById(R.id.textViewLon);
        txtMessage = (TextView) findViewById(R.id.textViewMessage);
        mainLayout = (LinearLayout) findViewById(R.id.mainLayout);
        layoutContent = (LinearLayout) findViewById(R.id.layoutContent);

        context = this;
        geoLocatorThingName = getIntent().getExtras().getString("thingname");
        getMyLocation();
        getCurrentTime();
        sharedPreferences = getSharedPreferences("sharedPrefs", MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();

        if (sharedPreferences.contains("prefUri")) {
            Toast.makeText(getApplicationContext(), "URI set", Toast.LENGTH_LONG).show();
        } else {
            editor.putString("prefUri", uri);
            editor.putString("prefAppKey", appKey);
            editor.commit();
        }

    }

    private void connectingToCloud() {

        ClientConfigurator config = new ClientConfigurator();
        config.setUri(uri);
        config.setReconnectInterval(15);
        config.getSecurityClaims().addClaim(RESTAPIConstants.PARAM_APPKEY, appKey);
        config.ignoreSSLErrors(true);
        config.setConnectTimeout(10000);

        try {
            client = new AndroidConnectedThingClient(config);
            client.start();
            connectionProgress = ProgressDialog.show(ShowLocations.this, "Please wait", "Connecting server...");
            new Thread(new Runnable() {
                @Override
                public void run() {

                    try {
                        Thread.sleep(10000);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    boolean isConnected = client.getEndpoint().isConnected();
                    if (isConnected) {
                        Message msg = Message.obtain();
                        Bundle b = new Bundle();
                        b.putString("state", "true");
                        msg.setData(b);
                        handler.sendMessage(msg);
                    } else {
                        Message msg = Message.obtain();
                        Bundle b = new Bundle();
                        b.putString("state", "false");
                        msg.setData(b);
                        handler.sendMessage(msg);
                    }

                }
            }).start();
        } catch (Exception e) {
            e.printStackTrace();
            Toast.makeText(getApplicationContext(), "" + e, Toast.LENGTH_LONG).show();
        }
    }


    Handler handler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            String state;
            state = msg.getData().getString("state");

            switch (state) {

                case "true":
                    receivingDataFromCloud();
                    break;
                case "false":
                    try {
                        client.disconnect();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    connectionProgress.dismiss();
                    Toast.makeText(getApplicationContext(), "Error in connection", Toast.LENGTH_LONG).show();
            }
        }
    };


    private void receivingDataFromCloud() {

        new Thread(new Runnable() {
            @Override
            public void run() {

                try {
                    Thread.sleep(5000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                ValueCollection paramsForGeoLocatorProperty = new ValueCollection();
                try {
                    InfoTable result = client.invokeService(RelationshipTypes.ThingworxEntityTypes.Things, geoLocatorThingName, "lastLocation", paramsForGeoLocatorProperty, 5000);
                    if (!result.isEmpty()) {
                        oldDate = result.getFirstRow().getStringValue("date");
                        oldLatitude = result.getFirstRow().getStringValue("latitude");
                        oldLongitude = result.getFirstRow().getStringValue("longitude");

                        Message msg = Message.obtain();
                        Bundle b = new Bundle();
                        b.putString("state", "true");
                        msg.setData(b);
                        handler1.sendMessage(msg);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                    Message msg = Message.obtain();
                    Bundle b = new Bundle();
                    b.putString("state", "false");
                    msg.setData(b);
                    handler1.sendMessage(msg);
                }
            }
        }).start();
    }

    Handler handler1 = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            String state;
            state = msg.getData().getString("state");

            switch (state) {
                case "true":
                    showValuesToDevice();
                    connectionProgress.dismiss();
                    break;
                case "false":
                    Toast.makeText(getApplicationContext(), "Error in connection", Toast.LENGTH_LONG).show();
                    connectionProgress.dismiss();
                    break;
            }
        }
    };


    private void sendingDataToCloud() {
        layoutContent.setVisibility(View.GONE);
        connectionProgress = ProgressDialog.show(ShowLocations.this, "Please wait", "Sending data to server...");
        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    Thread.sleep(5000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }

                ValueCollection paramsForLocation = new ValueCollection();
                paramsForLocation.put("date", newDatePrimitive);
                paramsForLocation.put("latitude", newLatitudePrimitive);
                paramsForLocation.put("longitude", newLongitudePrimitive);

                try {
                    client.invokeService(RelationshipTypes.ThingworxEntityTypes.Things, geoLocatorThingName, "setLocationProperty", paramsForLocation, 5000);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                handler2.sendEmptyMessage(0);
            }
        }).start();
    }

    Handler handler2 = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            layoutContent.setVisibility(View.VISIBLE);
            connectionProgress.dismiss();

        }
    };


    //Getting Current Location
    private void getMyLocation() {
        LocationTraker traker = new LocationTraker(context);

        if (traker.getState()) {
            Double latitude = traker.getlat();
            Double longitude = traker.getLon();

            newLatitude = Double.toString(latitude);
            newLongitude = Double.toString(longitude);
            newLatitudePrimitive = new StringPrimitive(newLatitude);
            newLongitudePrimitive = new StringPrimitive(newLongitude);

        }
    }

    public void getCurrentTime() {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        currentDateandTime = sdf.format(new Date());
        newDatePrimitive = new StringPrimitive(currentDateandTime);
    }

    private void showValuesToDevice() {
        mainLayout.setVisibility(View.VISIBLE);
        txtCurrentDate.setText(currentDateandTime);
        txtCurrentLatitude.setText(newLatitude);
        txtCurrentLongitude.setText(newLongitude);
        if (oldDate.equals("no-value")) {
            txtOldDate.setVisibility(View.GONE);
            txtLat.setVisibility(View.GONE);
            txtLon.setVisibility(View.GONE);
            txtOldtLatitude.setVisibility(View.GONE);
            txtOldLongitude.setVisibility(View.GONE);
            txtMessage.setVisibility(View.VISIBLE);
            txtMessage.setText("Previous location is empty.");
        } else {
            txtOldDate.setText(oldDate);
            txtOldtLatitude.setText(oldLatitude);
            txtOldLongitude.setText(oldLongitude);
        }
    }

    public void reportLocation(View v) {
        sendingDataToCloud();
    }


}
